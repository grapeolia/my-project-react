import React from 'react'
import { useSelector } from 'react-redux'
import MainLogo from '../icons/MainLogo'

function NavigationPanel() {
    const { favoriteProducts, productsInCart } = useSelector(
        (state) => state.products
    )

    return (
        <>
            <div className="navigationPanel container">
                <div className="navigationPanel__mainLogo">
                    <MainLogo />
                    <p className="navigationPanel__mainLogo-name">Store</p>
                </div>
                <ul className="navigationPanel__nav">
                    <li className="navigationPanel__nav-item">
                        <a href="/">Home</a>
                    </li>
                    <li className="navigationPanel__nav-item">
                        <a href="/cart">Cart ({productsInCart.length})</a>
                    </li>
                    <li className="navigationPanel__nav-item">
                        <a href="/favorites">
                            Favourite ({favoriteProducts.length})
                        </a>
                    </li>
                </ul>
            </div>
        </>
    )
}

export default NavigationPanel
