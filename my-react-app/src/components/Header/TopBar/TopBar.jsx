import React from 'react'
import SocialMediaLinks from './SocialMediaLinks'
import LoginUser from './LoginUser'
import CartPopup from './Cart/CartPopup'
import './TopBar.scss'

const TopBar = () => {
    return (
        <div className="headPanel">
            <div className="container">
                <SocialMediaLinks />
                <div className="userInfo">
                    <LoginUser />
                    <CartPopup />
                </div>
            </div>
        </div>
    )
}

export default TopBar
