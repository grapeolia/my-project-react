import React from 'react'

export default function LoginUser() {
    return (
        <div className="usersCredentials">
            <a href="#">Login</a>/<a href="#">Register</a>
        </div>
    )
}
