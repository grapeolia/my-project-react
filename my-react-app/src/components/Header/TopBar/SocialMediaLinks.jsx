import React from 'react'
import EmailIcon from '../../icons/EmailIcon'
import FacebookIcon from '../../icons/FacebookIcon'
import InstagramIcon from '../../icons/InstagramIcon'
import PinterestIcon from '../../icons/PnterestIcon'

export default function SocialMediaLinks() {
    return (
        <>
            <ul className="socialMedia">
                <li className="socialMedia__item">
                    <a href="#">
                        <FacebookIcon />
                    </a>
                </li>
                <li className="socialMedia__item">
                    <a href="#">
                        <PinterestIcon />
                    </a>
                </li>
                <li className="socialMedia__item">
                    <a href="#">
                        <InstagramIcon />
                    </a>
                </li>
                <li className="socialMedia__item">
                    <a href="#">
                        <EmailIcon />
                    </a>
                </li>
            </ul>
        </>
    )
}
