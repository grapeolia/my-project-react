import React from 'react'
import { useDispatch } from 'react-redux'
import { toggleInCart } from '../../../../store/actions'

const CartProductForPopUp = (props) => {
    const dispatch = useDispatch()
    const { product } = props

    const { id, name, price, pictureUrl } = product
    return (
        <>
            <li>
                <div className="cart-block-item">
                    <div className="cart-product">
                        <span className="cart-product__price">$ {price}</span>
                        <span className="cart-product__name">{name}</span>
                        <img
                            className="cart-product__image"
                            src={pictureUrl}
                            alt={name}
                        />
                    </div>
                    <button
                        onClick={() => dispatch(toggleInCart(id))}
                        className="cart-block-item__btn-rm"
                    >
                        X
                    </button>
                </div>
            </li>
        </>
    )
}

export default CartProductForPopUp
