import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import CartBody from './CartBody'
import CartIcon from '../../../icons/CartIcon'
import { toggleCartShowing } from '../../../../store/actions'

export default function CartPopup() {
    const isCartOpened = useSelector((state) => state.products.currentModal)
    const productsInCart = useSelector((state) => state.products.productsInCart)
    const dispatch = useDispatch()

    return (
        <>
            <div
                className="cartItem"
                onMouseOver={() => dispatch(toggleCartShowing(true))}
            >
                <CartIcon />
                <p>Cart ({productsInCart.length})</p>
                {isCartOpened && <CartBody />}
            </div>
        </>
    )
}
