import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CartProductForPopUp from './CartProductForPopUp'
import { toggleCartShowing } from '../../../../store/actions'

const CartBody = () => {
    const dispatch = useDispatch()
    const productsInCart = useSelector((state) => state.products.productsInCart)

    const totalCost =
        Math.round(
            productsInCart.reduce((acc, product) => acc + product.price, 0) *
                100
        ) / 100

    return (
        <>
            <div
                className="cart-pop-up"
                onMouseLeave={() => dispatch(toggleCartShowing(false))}
            >
                <ul className="cart-block">
                    {productsInCart.length > 0 &&
                        productsInCart.map((product) => (
                            <CartProductForPopUp
                                key={product.id + '---cart'}
                                product={product}
                            />
                        ))}
                </ul>
                <div className="cartFooter">
                    <p className="cartFooter__total-amount">
                        Total cost $ {totalCost}
                    </p>

                    <div className="cartFooter__actionButtons">
                        <a href="/cart">View Cart</a>
                        <a href="/#">Proceed to Checkout</a>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CartBody
