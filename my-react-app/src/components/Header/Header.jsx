import React from 'react'
import NavigationPanel from './NavigationPanel'
import Advertisement from './Advertisement'
import TopBar from './TopBar/TopBar'
import './Header.scss'

const Header = () => {
    return (
        <>
            <TopBar />
            <NavigationPanel />
            <Advertisement />
        </>
    )
}

export default Header
