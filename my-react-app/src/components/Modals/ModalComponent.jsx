import React, { useContext } from 'react'
import './ModalStyles.css'
import 'bootstrap/dist/css/bootstrap.css'
import { ModalContext } from '../../store/ModalContext'

const ModalComponent = () => {
    const { toggleModal, currentModal } = useContext(ModalContext)

    const { title, description } = currentModal.modal
    const { actionOnOk, paramsForAction } = currentModal

    const proceedAndClose = () => {
        actionOnOk(paramsForAction)
        toggleModal()
    }

    return (
        <>
            {
                <>
                    <div
                        onClick={() => toggleModal()}
                        className="shadow-block"
                    ></div>
                    <div
                        className="modal modal-sheet position-fixed d-block"
                        tabIndex="-1"
                        role="dialog"
                        id="modalSheet"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content rounded-4 shadow">
                                <div className="modal-header border-bottom-0">
                                    <h1 className="modal-title fs-5">
                                        {title}
                                    </h1>
                                    <button
                                        onClick={() => toggleModal()}
                                        type="button"
                                        className="btn-close"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                    ></button>
                                </div>
                                <div className="modal-body py-0">
                                    <p>{description}</p>
                                </div>
                                <div className="modal-footer flex-column border-top-0">
                                    <button
                                        onClick={() => proceedAndClose()}
                                        type="button"
                                        className="btn btn-lg btn-primary w-100 mx-0 mb-2"
                                    >
                                        Proceed
                                    </button>
                                    <button
                                        onClick={() => toggleModal()}
                                        type="button"
                                        className="btn btn-lg btn-light w-100 mx-0"
                                        data-bs-dismiss="modal"
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            }
        </>
    )
}

export default ModalComponent
