import React from 'react'
import ProductCard from './ProductCard'

export default function ProductList(props) {
    const { products } = props
    return (
        <>
            <ul className="products-block">
                {products &&
                    products.map((product) => {
                        return (
                            <ProductCard key={product.id} product={product} />
                        )
                    })}
            </ul>
        </>
    )
}
