import React, { useContext } from 'react'
import LikeButtonIcon from '../icons/LikeButtonIcon'
import ButtonToCallModal from '../Buttons/ButtonToCallModal'
import './ProductCard.scss'
import { useParams } from 'react-router-dom'
import { ModalContext } from '../../store/ModalContext'
import { FavoriteProductsContext } from '../../store/FavoriteProductsContext'
import { ProductsInCartContext } from '../../store/ProductsInCartContext'

const ProductCard = (props) => {
    const { product } = props
    const { favoriteProducts, toggleProductInFavorites } = useContext(
        FavoriteProductsContext
    )
    const { toggleProductInCart, productsInCart } = useContext(
        ProductsInCartContext
    )

    const { cart } = useParams()
    const { id, pictureUrl, name, color, price } = product
    const isFavorite = !!favoriteProducts.find((product) => product.id === id)
    const isInCart = !!productsInCart.find((product) => product.id === id)

    return (
        <>
            <li>
                <div className="product">
                    <img
                        className="product__image"
                        src={pictureUrl}
                        alt={name}
                    />
                    <div className="product__info">
                        <div>
                            <p className="product__name">{name}</p>
                            <span onClick={() => toggleProductInFavorites(id)}>
                                <LikeButtonIcon
                                    fill={isFavorite ? 'red' : ''}
                                />
                            </span>
                        </div>
                        <p className="product__description-color">{color}</p>
                        <div className="product__footer">
                            <p className="product__price">$ {price}</p>

                            <ButtonToCallModal
                                className={
                                    cart
                                        ? 'product__btnAddToCart'
                                        : isInCart
                                        ? 'product__btnAddToCart product__btnAddToCart-disabled'
                                        : 'product__btnAddToCart'
                                }
                                modalID={isInCart ? 'modalID2' : 'modalID1'}
                                text={
                                    isInCart ? 'Already in Cart' : 'Add to Cart'
                                }
                                actionOnOk={toggleProductInCart}
                                paramsForAction={id}
                            />
                        </div>
                    </div>
                </div>
            </li>
        </>
    )
}

export default ProductCard
