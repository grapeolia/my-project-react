import React from 'react'
import { useDispatch } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.css'
import { toggleModal } from '../../store/actions'

const ButtonToCallModal = (props) => {
    const dispatch = useDispatch()
    const { modalID, text, actionOnOk, paramsForAction, className } = props

    return (
        <>
            <button
                className={className}
                type="button"
                onClick={() =>
                    dispatch(toggleModal(modalID, actionOnOk, paramsForAction))
                }
            >
                {text}
            </button>
        </>
    )
}

export default ButtonToCallModal
