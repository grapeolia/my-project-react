import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { Provider } from 'react-redux'
import store from './store/store'

store.subscribe(() => console.log(store.getState()))
console.log(store)
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <Provider store={store}>
                <App />
            </Provider>
        </BrowserRouter>
    </React.StrictMode>
)

/*
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <ModalProvider>
            <ProductsProvider>
                <ProductsInCartProvider>
                    <FavoriteProductsProvider>
                        <BrowserRouter>
                            <App />
                        </BrowserRouter>
                    </FavoriteProductsProvider>
                </ProductsInCartProvider>
            </ProductsProvider>
        </ModalProvider>
    </React.StrictMode>
)
*/

reportWebVitals()
