export const loadAllProducts = () => {
    return fetch('/data/goodsDeclarations.json').then((response) =>
        response.json()
    )
}

export const getProductById = (productId) => {
    return (
        loadAllProducts().then((goods) =>
            goods.find((good) => good.id === productId)
        ) || null
    )
}
