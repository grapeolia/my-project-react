export const getModals = () => {
    return fetch('/data/modalWindowsDeclarations.json').then((response) =>
        response.json()
    )
}

export const getModal = (modalID) => {
    return (
        getModals().then((modals) =>
            modals.find((modal) => modal.id === modalID)
        ) || null
    )
}
