import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Routes } from 'react-router-dom'
import ModalComponent from './components/Modals/ModalComponent'
import Header from './components/Header/Header'
import ProductList from './components/Products/ProductList'
import Error from './components/Error/Error'
import { Switch } from '@mui/material'

const App = () => {
    const currentModal = useSelector((state) => state.modal.currentModal)
    const isLoaded = useSelector((state) => state.products.isLoaded)

    return (
        <>
            {currentModal && <ModalComponent />}
            <Header />
            {isLoaded && (
                <Routes>
                    <Switch>
                        <Route path="/" element={<ProductList />} />
                        <Route path="/:cart" element={<ProductList />} />
                        <Route path="/:favorites" element={<ProductList />} />
                        <Route path="*" element={<Error />} />
                    </Switch>
                </Routes>
            )}
        </>
    )
}

export default App
