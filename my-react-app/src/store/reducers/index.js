import { productsReducer } from './productReducer'
import { modalReducer } from './modalReducer'
import { combineReducers } from 'redux'

const allReducers = combineReducers({
    products: productsReducer,
    modal: modalReducer,
})

export default allReducers
