import { getModal } from '../../api/modals'

const initialState = {
    currentModal: null,
    isModalLoaded: false,
}

export const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'TOGGLE_MODAL':
            return {
                ...state,
                currentModal: getModal(action.payload.modal),
                isModalLoaded: !!action.payload.modal,
            }
        default:
            return state
    }
}
