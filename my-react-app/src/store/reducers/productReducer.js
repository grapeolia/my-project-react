const initialState = {
    products: [],
    favoriteProducts:
        JSON.parse(localStorage.getItem('favoriteProducts')) || [],
    productsInCart: JSON.parse(localStorage.getItem('cartProducts')) || [],
    isLoading: false,
    hasError: false,
    isCartShown: false,
}

export const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_ALL_PRODUCTS_REQUEST': {
            return {
                ...state,
                isLoading: true,
            }
        }
        case 'GET_ALL_PRODUCTS_ON_SUCCESS': {
            return {
                ...state,
                products: action.payload,
                isLoading: false,
                hasError: false,
            }
        }
        case 'GET_ALL_PRODUCTS_ERROR': {
            return {
                ...state,
                isLoading: false,
                hasError: true,
            }
        }
        case 'TOGGLE_FAVORITE': {
            const product = state.product.find(
                (product) => product.id === action.payload.id
            )
            const isFavorite = state.favoriteProducts.find(
                (product) => product.id === action.payload.id
            )
            return {
                ...state,
                favoriteProducts: isFavorite
                    ? [
                          ...state.favoriteProducts.filter(
                              (product) => product.id !== action.payload.id
                          ),
                      ]
                    : [...state.favoriteProducts, product],
            }
        }
        case 'TOGGLE_IN_CART': {
            const product = state.product.find(
                (product) => product.id === action.payload.id
            )
            const isInCart = state.productsInCart.find(
                (product) => product.id === action.payload.id
            )
            return {
                ...state,
                productsInCart: isInCart
                    ? [
                          ...state.productsInCart.filter(
                              (product) => product.id !== action.payload.id
                          ),
                      ]
                    : [...state.productsInCart, product],
            }
        }
        case 'TOGGLE_CART_SHOWING': {
            return {
                ...state,
                isCartShown: action.payload || false,
            }
        }
        default: {
            return state
        }
    }
}
