import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import allReducers from './reducers'
import thunk from 'redux-thunk'

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : (f) => f

const store = createStore(
    allReducers,
    compose(applyMiddleware(thunk), devTools)
)

export default store
