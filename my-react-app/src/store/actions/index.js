import { loadAllProducts } from '../../api/products'

export const toggleModal = (
    modal = null,
    actionOnOk = null,
    paramsForAction = null
) => {
    return {
        type: 'TOGGLE_MODAL',
        payload: { modal, actionOnOk, paramsForAction },
    }
}

export const getAllProducts = (products) => {
    return {
        type: 'GET_ALL_PRODUCTS_ON_SUCCESS',
        payload: products,
    }
}

export const fetchProducts = () => async (dispatch) => {
    dispatch({ type: 'GET_ALL_PRODUCTS_REQUEST' })
    return loadAllProducts()
        .then((products) => {
            dispatch({ type: 'GET_ALL_PRODUCTS_ON_SUCCESS', payload: products })
        })
        .catch((response) => {
            dispatch({
                type: 'GET_ALL_PRODUCTS_ERROR',
                payload: response.error,
            })
        })
}

export const toggleFavorite = (id) => {
    return { type: 'TOGGLE_FAVORITE', payload: { id } }
}

export const toggleInCart = (id) => {
    return { type: 'TOGGLE_IN_CART', payload: { id } }
}

export const toggleCartShowing = (show = false) => {
    return { type: 'TOGGLE_CART_SHOWING', payload: { show } }
}
